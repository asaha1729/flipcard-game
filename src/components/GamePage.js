import React, { Component } from 'react'
import Board from './Board'
import { Button, Result } from 'antd'

export default class GamePage extends Component {
    constructor(props){
        super()
        this.state = {
            score: 0,
            gameWon: false
        }
        this.total = (props.data * props.data) / 2
    }

    setScore = (v) => {
        if(v === this.total) {
            this.setState({ gameWon: true, score: v })
        } else this.setState({ score : v })
    }

    render() {
        return (
            <div>
                <div className="dash-base-l" />
                <div className="game-base">
                    <div className="game-page-header">
                        <div className="game-page-header-scoreboard">
                            {/* <div className="score-title">
                                <p>SCORE</p>
                            </div> */}
                            <div className="score-display">
                                <div className="score-display-value">
                                    <p>{this.state.score}</p>
                                </div>
                                <div className="score-display-total">
                                    <p>/{this.total}</p>
                                </div>
                            </div>
                        </div>
                        <div className="game-page-header-back">
                            <Button className="back-button" onClick={() => this.props.goToComponent('Dash')} >
                                 {
                                     this.state.gameWon ? "Play Again" : "Give Up"
                                 }
                            </Button>
                        </div>
                    </div>
                    {
                        this.state.gameWon
                        ? <Result 
                            className="game-won-result"
                            status="success" 
                            title="Congratulations Stranger!! You've finally won." 
                            subTitle="Now go back and Play again if you dare!!"
                            />
                        : <Board score={this.setScore} size={this.props.data} />
                    }
                </div>
                <div className="dash-base-r" />
            </div>
        )
    }
}