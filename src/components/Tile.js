import React, { Component } from 'react'
import Images from './Images'

export default class Tile extends Component {
    constructor() {
        super()
        this.state = {
            clicked: false
        }
    }

    componentWillReceiveProps() {
        this.setState({ clicked: false })
    }

    getCardStyle = () => {
        if(this.props.tileInfo.disabled) {
            return {
                transform: 'rotateY(180deg)'
            }
        } else if(this.props.tileInfo.selected || this.state.clicked) return { transform: 'rotateY(180deg)' }
          else return null
    }

    callAfter = () => {
        if(!this.props.tileInfo.disabled) {
            window.setTimeout(() => this.props.tileClicked(this.props.row, this.props.col), 1000)
        }
    }

    render() {
        const { length, tileInfo } = this.props
        return (
            <div style={{ height: length + 'vh', width: length + 'vh' }} className="tile-wrapper" >
                <div onClick={() => this.setState({ clicked: true }, this.callAfter)} class="flip-card">
                    <div class="flip-card-inner" style={this.getCardStyle()}>
                        <div class="flip-card-front">
                            {/* <div className="tile-face">
                                {tileInfo.num}
                            </div>                             */}
                        </div>
                            <div class="flip-card-back">
                                <Images tileClicked={this.props.tileClicked} {...this.props} />
                            </div>
                        </div>
                    </div>
                </div>
                )
            }
}