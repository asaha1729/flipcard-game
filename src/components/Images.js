import React, { Component } from 'react'

export default class Images extends Component {
    img_1 = require('../assets/apple.jpg')
    img_2 = require('../assets/bananas.jpg')
    img_3 = require('../assets/bark.jpg')
    img_4 = require('../assets/blue-flowers.jpg')
    img_5 = require('../assets/bread.jpg')
    img_6 = require('../assets/calculator.jpg')
    img_7 = require('../assets/cartoon-helicopter.jpg')
    img_8 = require('../assets/cocktail-drink.jpg')
    img_9 = require('../assets/coins.jpg')
    img_10 = require('../assets/december.jpg')
    img_11 = require('../assets/dog.jpeg')
    img_12 = require('../assets/eagle.jpg')
    img_13 = require('../assets/forklift.jpg')
    img_14 = require('../assets/frog.jpg')
    img_15 = require('../assets/hamburger.jpg')
    img_16 = require('../assets/hand-flower.jpeg')
    img_17 = require('../assets/headphone.jpg')
    img_18 = require('../assets/kiwi.jpg')
    img_19 = require('../assets/macaroons.jpg')
    img_20 = require('../assets/mela.jpg')
    img_21 = require('../assets/mitze.jpg')
    img_22 = require('../assets/motorcycle.jpg')
    img_23 = require('../assets/orange-bottle.jpg')
    img_24 = require('../assets/orange.jpg')
    img_25 = require('../assets/peacock.jpg')
    img_26 = require('../assets/pear.jpg')
    img_27 = require('../assets/raspberries.jpg')
    img_28 = require('../assets/red-car.jpg')
    img_29 = require('../assets/red-ribbon.jpg')
    img_30 = require('../assets/spinning.jpg')
    img_31 = require('../assets/sports-car.jpg')
    img_32 = require('../assets/starfish.jpg')

    render() {
        const { length, row, col, tileInfo } = this.props
        return (
            <img
                onClick={() => this.props.tileClicked(row, col)}
                src={this['img_' + this.props.tileInfo.num]}
                alt={this.props.tileInfo.num}
                style={{ height: length + 'vh', width: length + 'vh', filter: tileInfo.disabled ? 'grayScale(1)' : null }}
            />
        )
    }
}