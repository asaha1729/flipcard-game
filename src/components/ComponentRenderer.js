import React, { Component } from 'react'
import '../styles/main.scss'
import Dash from './Dash'
import GamePage from './GamePage'

export default class ComponentRenderer extends Component {
    state = {
        showDash: true,
        data: null
    }
    goToComponent = (comp, value=null) => {
        this.setState({ showDash: !this.state.showDash, data: value })
    }
    render(){
        return(
            <div>{ this.state.showDash 
                ? <Dash goToComponent={this.goToComponent} /> 
                : <GamePage data={this.state.data} goToComponent={this.goToComponent} />  }
            </div>
        )
    }
}