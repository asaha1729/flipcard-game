import { Col, message, Row } from 'antd';
import React, { Component } from 'react';
import Tile from './Tile';

export default class Board extends Component{
    constructor(props) {
        super();
        this.state = {
            tileArray: [],
            tileInfoArray: [],
            selectedTile: [-1,-1],
            correctlyIdentifiedTiles: []
        }
    }

    componentDidMount() {
        this.setUpBoard()
    }

    setUpBoard = () => {
        // method for implementing board logic
        const c = this.props.size
        let tileInfoArray = []
        let valuesArray = this.getValuesArray(c * c)
        this.shuffleArray(valuesArray)
        console.log(valuesArray)
        let r_val = 0
        for(let i=0; i<c;i++) {
            tileInfoArray[i] = []
            for(let j=0; j<c; j++) {
                tileInfoArray[i][j] = {
                    num: valuesArray[r_val++],
                    selected: false,
                    disabled: false
                }
            }                  
        }
        this.setState({ tileInfoArray },this.renderTiles)        
    }

    renderTiles = () => {
        let tileArray = []
        let tileInfoArray = this.state.tileInfoArray
        const c = this.props.size
        for(let i=0; i<c;i++) {
            let tileRow = []
            for(let j=0; j<c; j++) {
                let spanValue = 24 / c
                tileRow.push(
                    <Col span={spanValue}>
                        <Tile 
                            tileInfo={tileInfoArray[i][j]} 
                            length={spanValue*3} row={i} col={j} 
                            tileClicked={this.tileClicked}
                        />
                    </Col>
                )
            }
            tileArray.push(
                <Row>
                    {tileRow}
                </Row>
            )
        }
        this.setState({tileArray})
    }

    getValuesArray = (c) => {
        let a = []
        c = c/2
        for(let k=1; k<=c ; k++) {
            a[k-1] = k
            a[k-1+c] = k
        }
        console.log("valuesArray", a)
        return a
    }

    shuffleArray = (array) => {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    tileClicked = (row, col) => {
        let  { tileInfoArray, selectedTile, correctlyIdentifiedTiles} = this.state
        if(selectedTile[0] === -1) {
            selectedTile = [row,col]
            tileInfoArray[row][col].selected = true
        } else {
            if (row === selectedTile[0] && col === selectedTile[1]){
                message.warning("Same Tile!!")
            } else if( tileInfoArray[row][col].num === tileInfoArray[selectedTile[0]][selectedTile[1]].num) {
                //Tiles match
                tileInfoArray[row][col].disabled = true
                tileInfoArray[selectedTile[0]][selectedTile[1]].disabled = true
                correctlyIdentifiedTiles.push(tileInfoArray[row][col].num)
                this.props.score(correctlyIdentifiedTiles.length)
                selectedTile = [-1,-1]
                message.success("Matched!!")
            } else {
                //Tiles do not match
                tileInfoArray[row][col].selected = false
                tileInfoArray[selectedTile[0]][selectedTile[1]].selected = false
                selectedTile = [-1,-1]
                message.error("Not matched!!")
            }
        }
        this.setState({ tileInfoArray, selectedTile, correctlyIdentifiedTiles }, this.renderTiles)
    }

    render() {
        return(
            <div className="board-wrapper">
                <div className="board-box">
                    <Col>
                        {this.state.tileArray}
                    </Col>
                </div>
            </div>
        )
    }
}