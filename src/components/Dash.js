import React, { Component } from 'react'
// import 'antd/dist/antd.css';
import { Row, Col, Button } from 'antd';

export default class Dash extends Component {
    boardSelect = (size) => {
        this.props.goToComponent('board', size)
    }
    render() {
        return (
            <div>
                <div className="dash-base-l">
                    {/* <div className="title-flip-box">
                        <p className="title-flip-horizontal" >F L I P</p>
                    </div> */}
                    <div className="dash-box box">
                        {/* <Col span={20} className="left-col" /> */}
                        <Col className="dash-content">
                            {/* <Row className="top-row" /> */}

                            <Row className="content-row">
                                <div className="box-menu">
                                    <h1 style={{ color: '#fff' }}>FLIP</h1>
                                    <div className="box-menu-item">
                                        <Button onClick={() => this.boardSelect(4)} className="box-menu-button">
                                            4 x 4
                                        </Button>
                                    </div>
                                    <div className="box-menu-item">
                                        <Button onClick={() => this.boardSelect(6)} className="box-menu-button">
                                            6 x 6
                                        </Button>
                                    </div>
                                    <div className="box-menu-item">
                                        <Button onClick={() => this.boardSelect(8)} className="box-menu-button">
                                            8 x 8
                                        </Button>
                                    </div>
                                </div>
                            </Row>
                            {/* <Row className="bottom-row" /> */}
                        </Col>
                        {/* <Col span={4} className="right-col" /> */}
                    </div>
                </div>
                <div className="dash-base-r" />
            </div>
        )
    }
}