# FlipCard

This is a memory teaser game where you flip and match twin tiles. Has 3 levels of difficulty. 

## Project info

This game is built with React.js and other supporting node.js libraries. 

### Run info

Run the game on your browser with the following command.
```
npm start
```

## Authors

Asmita Saha
